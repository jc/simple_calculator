
def add(a ,b):
    return a + b

def subtract(a ,b):
    return a - b

def multiply(a, b):
    return a * b

def divide(a, b):
    return a / b

def more():
    print("Would you like to calculate more? (y/n) ")
    answer = input(c)
    if answer == 'y':
        print("What would you like to do: add, subtract, multiply or divide?")
        code = input(c)
    else:
        print("Stopping computing sequence ... Goodbye!")
        exit()

def addition():
    print(f"Compute {x} + {y}?(y/n) ")
    answer = input(c)
    if answer == 'y':
        result = add(x, y)
        print(f"{x} + {y} =", result, "\n")
    if answer == 'n':
        print("Choose different method: press m\nExit: press e")
        answer = input(z)
        if answer == 'm':
            print("Press RETURN to continue")
            input(z)
        elif answer == 'e':
            print("Stopping computing sequence ... Goodbye!")
            exit()

def subtraction():
    print(f"Compute {x} - {y}?(y/n) ")
    answer = input(c)
    if answer == 'y':
        result = subtract(x, y)
        print(f"{x} - {y} =", result, "\n")
    if answer == 'n':
        print("Choose different method: press m\nExit: press e")
        answer = input(z)
        if answer == 'm':
            print("Press RETURN to continue")
            input(z)
        elif answer == 'e':
            print("Stopping computing sequence ... Goodbye!")
            exit()

def multiplication():
    print(f"Compute {x} * {y}?(y/n) ")
    answer = input(c)
    if answer == 'y':
        result = multiply(x, y)
        print(f"{x} * {y} =", result, "\n")
    if answer == 'n':
        print("Choose different method: press m\nExit: press e")
        answer = input(z)
        if answer == 'm':
            print("Press RETURN to continue")
            input(c)
        elif answer == 'e':
            print("Stopping computing sequence ... Goodbye!")
            exit()

def division():
    print(f"Compute {x} / {y}?(y/n) ")
    answer = input(c)
    if answer == 'y':
        result = divide(x, y)
        print(f"{x} / {y} =", result, "\n")
    if answer == 'n':
        print("Choose different method: press m\nExit: press e")
        answer = input(c)
        if answer == 'm':
            print("Press RETURN to continue")
            input(z)
        elif answer == 'e':
            print("Stopping computing sequence ... Goodbye!")
            exit()

z = 'First Number > '
w = 'Second Number > '
c = '> '

print("Welcome to the Simple Calculator\n")
input('Press RETURN >  \n')
print("This calculator can process two numbers\n")
input('Press RETURN > \n')
print("What would you like to do: add, subtract, multiply or divide?")

code = input()

while code:


    if code == 'add':
        print("Type in the numbers you would like to add\n")
        x = float(input(z))
        y = float(input(w))

        addition()
        more()


    elif code == 'subtract':
        print("You chose SUBTRACT")
        print("Type in the numbers you would like to subtract\n")
        x = float(input(z))
        y = float(input(w))
        subtraction()
        more()

    elif code == 'multiply':
        print("You chose MULTIPLY")
        print("Type in the numbers you would like to multiply\n")
        x = float(input(z))
        y = float(input(w))
        multiplication()
        more()

    elif code == 'divide':
        print(" You chose DIVIDE")
        print("Type in the numbers you would like to divide\n")
        x = float(input(z))
        y = float(input(w))
        division()
        more()

    elif code == 'exit':
        print("Exiting program ... Goodbye!")
        exit()

    else:
        print("What would you like to do: add, subtract, multiply or divide?")

        code = input()
